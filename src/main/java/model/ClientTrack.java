package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ClientTrack {
    @JsonProperty("author")
    String author;
    @JsonProperty("title")
    String title;
    @JsonProperty("album")
    String album;
    @JsonProperty("cover")
    String cover;
    @JsonProperty("duration")
    int duration;
    @JsonProperty("durationHuman")
    String durationHuman;
    @JsonProperty("url")
    String url;
    @JsonProperty("id")
    String id;
    @JsonProperty("isVideo")
    boolean isVideo;
    @JsonProperty("isAdvertisement")
    boolean isAdvertisement;
    @JsonProperty("inLibrary")
    boolean inLibrary;
}

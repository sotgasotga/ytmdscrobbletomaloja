package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ClientPlayer {
    @JsonProperty("hasSong")
    boolean hasSong;
    @JsonProperty("isPaused")
    boolean isPaused;
    @JsonProperty("volumePercent")
    int volumePercent;
    @JsonProperty("seekbarCurrentPosition")
    int seekbarCurrentPosition;
    @JsonProperty("seekbarCurrentPositionHuman")
    String seekbarCurrentPositionHuman;
    @JsonProperty("statePercent")
    double statePercent;
    @JsonProperty("likeStatus")
    String likeStatus;
    @JsonProperty("repeatType")
    String repeatType;
}

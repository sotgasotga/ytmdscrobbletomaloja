package model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ServerTrack {
    String artist;
    String title;
    String album;
    int duration;
}

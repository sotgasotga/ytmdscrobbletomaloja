import com.fasterxml.jackson.databind.ObjectMapper;
import model.ClientPlayer;
import model.ClientTrack;
import model.ServerTrack;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        if (args.length != 2 && args[0].equals("") && args[1].equals("")) {
            System.exit(-1);
        } else {
            new Main().connectionDetectionLoop(args);
        }

    }

    private void connectionDetectionLoop(String[] args) throws InterruptedException {
        //noinspection InfiniteLoopStatement
        for(;;) {
            try (Socket s = new Socket("localhost", 9863)) {
                songDetectionLoop(args);
            } catch (IOException | InterruptedException ex) {
                System.err.println("Could not connect to YTMDP, Retying in 30 Seconds.");
            }
            Thread.sleep(30000);
        }
    }

    private void songDetectionLoop(String[] args) throws InterruptedException, IOException {
        System.out.println("Started Detection");
        //noinspection StatementWithEmptyBody
        for(;;) {
            new Main().detectIfSongIsPlaying(args[0], args[1]);
            Thread.sleep(10000);
        }
    }

    public void detectIfSongIsPlaying(String url, String key) throws IOException, InterruptedException {
        ClientPlayer clientPlayer = getPlayer();
        if (clientPlayer.isHasSong() && !clientPlayer.isPaused() && clientPlayer.getSeekbarCurrentPosition() < 10) {
            scrobble(convertClientToServer(), url, key);
        }
    }

    public ClientPlayer getPlayer() throws IOException {
        return new ObjectMapper().readValue(new URL("http://localhost:9863/query/player"), ClientPlayer.class);
    }
    public ClientTrack getTrack() throws IOException {
        return new ObjectMapper().readValue(new URL("http://localhost:9863/query/track"), ClientTrack.class);
    }

    public ServerTrack convertClientToServer() throws IOException {
        ClientTrack clientTrack = getTrack();
        ServerTrack serverTrack = new ServerTrack();
        serverTrack.setArtist(clientTrack.getAuthor());
        serverTrack.setTitle(clientTrack.getTitle());
        if (clientTrack.getAlbum().isEmpty()) {
            serverTrack.setAlbum(clientTrack.getAlbum());
        }
        serverTrack.setDuration(clientTrack.getDuration());
        return serverTrack;
    }

    public void scrobble(ServerTrack serverTrack, String url, String key) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + "/apis/mlj_1/newscrobble?key=" + key))
                .POST(HttpRequest.BodyPublishers.ofString(new ObjectMapper().writeValueAsString(serverTrack)))
                .header("Content-type", "application/json")
                .build();
        client.send(request, HttpResponse.BodyHandlers.discarding()).statusCode();
        HttpResponse<?> response = client.send(request, HttpResponse.BodyHandlers.discarding());
        System.out.println("Status: " + response.statusCode() + " | Song: " + serverTrack.getArtist() + " - " + serverTrack.getTitle());
    }
}
